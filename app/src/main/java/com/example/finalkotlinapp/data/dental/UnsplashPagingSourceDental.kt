package com.example.finalkotlinapp.data.dental

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.finalkotlinapp.api.UnsplashApi
import retrofit2.HttpException
import java.io.IOException

private const val UNSPLASH_STARTING_PAGE_INDEX = 1

class UnsplashPagingSourceDental(
    private val unsplashApi: UnsplashApi,
    private val query: String
) : PagingSource<Int, UnsplashDentalClinicData>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UnsplashDentalClinicData> {
        val position = params.key ?: UNSPLASH_STARTING_PAGE_INDEX

        return try {

            val response = unsplashApi.dentalClinicPhotos(query, position, params.loadSize)
            val dentalphoto = response.results

            LoadResult.Page(
                data = dentalphoto,
                prevKey = if (position == UNSPLASH_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (dentalphoto.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, UnsplashDentalClinicData>): Int? {

        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }

    }


}