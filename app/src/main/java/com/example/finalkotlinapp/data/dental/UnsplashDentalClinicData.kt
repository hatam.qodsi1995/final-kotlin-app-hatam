package com.example.finalkotlinapp.data.dental

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class UnsplashDentalClinicData(
    val id: String,
    val user: UnsplashUserDental,
    val urls: UnsplashDentalPhotoUrls,
) : Parcelable {

    @Parcelize
    data class UnsplashUserDental(
        val username: String
    ) : Parcelable {
        val attributionUrl get() = "https://unsplash.com/$username?utm_source=ImageSearchApp&utm_medium=referral"
    }

    @Parcelize
    data class UnsplashDentalPhotoUrls(
        val regular: String,
    ) : Parcelable {

    }

}