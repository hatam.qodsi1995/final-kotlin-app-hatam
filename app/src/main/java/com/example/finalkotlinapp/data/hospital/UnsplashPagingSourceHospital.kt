package com.example.finalkotlinapp.data.hospital

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.finalkotlinapp.api.UnsplashApi
import retrofit2.HttpException
import java.io.IOException

private const val UNSPLASH_STARTING_PAGE_INDEX = 1

class UnsplashPagingSourceHospital(
    private val unsplashApi: UnsplashApi,
    private val query: String
) : PagingSource<Int, UnsplashHospitalData>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UnsplashHospitalData> {
        val position = params.key ?: UNSPLASH_STARTING_PAGE_INDEX

        return try {

            val response = unsplashApi.hospitalPhotos(query, position, params.loadSize)
            val hospitalPhoto = response.results

            LoadResult.Page(
                data = hospitalPhoto,
                prevKey = if (position == UNSPLASH_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (hospitalPhoto.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, UnsplashHospitalData>): Int? {

        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }

    }


}