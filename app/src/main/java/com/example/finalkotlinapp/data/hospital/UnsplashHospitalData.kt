package com.example.finalkotlinapp.data.hospital

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UnsplashHospitalData(
    val id: String,
    val user: UnsplashUser,
    val urls: UnsplashPhotoUrls,
) : Parcelable {

    @Parcelize
    data class UnsplashUser(
        val username: String
    ) : Parcelable {
        val attributionUrl get() = "https://unsplash.com/$username?utm_source=ImageSearchApp&utm_medium=referral"
    }

    @Parcelize
    data class UnsplashPhotoUrls(
        val regular: String,
    ) : Parcelable {

    }

}