package com.example.finalkotlinapp.ui.dentalclinic

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.finalkotlinapp.R
import com.example.finalkotlinapp.data.dental.UnsplashDentalClinicData
import com.example.finalkotlinapp.databinding.ItemOfRecyclerDentalBinding

class UnsplashDentalPhotoAdapter : PagingDataAdapter<UnsplashDentalClinicData,
        UnsplashDentalPhotoAdapter.PhotoViewHolder>(PHOTO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding = ItemOfRecyclerDentalBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {

        val currentItem = getItem(position)

        currentItem?.let { holder.bind(it) }

    }


    class PhotoViewHolder(private val binding: ItemOfRecyclerDentalBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(photo: UnsplashDentalClinicData) {
            binding.apply {
                Glide.with(itemView)
                    .load(photo.urls.regular)
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_error)
                    .into(dentalImageView)

                dentalUserName.text = photo.user.username
            }
        }
    }

    companion object {
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<UnsplashDentalClinicData>() {
            override fun areItemsTheSame(
                oldItem: UnsplashDentalClinicData,
                newItem: UnsplashDentalClinicData
            ) = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: UnsplashDentalClinicData,
                newItem: UnsplashDentalClinicData
            ) = oldItem.id == newItem.id

        }
    }

}