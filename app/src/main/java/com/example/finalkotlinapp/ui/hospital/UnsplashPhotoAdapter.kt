package com.example.finalkotlinapp.ui.hospital

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.finalkotlinapp.R
import com.example.finalkotlinapp.data.hospital.UnsplashHospitalData
import com.example.finalkotlinapp.databinding.ItemOfRecyclerHospitalBinding

class UnsplashPhotoAdapter : PagingDataAdapter<UnsplashHospitalData,
        UnsplashPhotoAdapter.PhotoViewHolder>(PHOTO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding = ItemOfRecyclerHospitalBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {

        val currentItem = getItem(position)

        currentItem?.let { holder.bind(it) }

    }


    class PhotoViewHolder(private val binding: ItemOfRecyclerHospitalBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(photo: UnsplashHospitalData) {
            binding.apply {
                Glide.with(itemView)
                    .load(photo.urls.regular)
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_error)
                    .into(hospitalImageView)

                hospitalUserName.text = photo.user.username

            }
        }
    }

    companion object {
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<UnsplashHospitalData>() {
            override fun areItemsTheSame(
                oldItem: UnsplashHospitalData,
                newItem: UnsplashHospitalData
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: UnsplashHospitalData,
                newItem: UnsplashHospitalData
            ) = oldItem.id == newItem.id

        }
    }

}

