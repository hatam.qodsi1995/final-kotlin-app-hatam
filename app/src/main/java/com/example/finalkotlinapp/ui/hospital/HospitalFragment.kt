package com.example.finalkotlinapp.ui.hospital

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.finalkotlinapp.R
import com.example.finalkotlinapp.databinding.FragmentHospitalBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HospitalFragment : Fragment(R.layout.fragment_hospital) {

    private val viewModel by viewModels<HospitalViewModel>()

    private var _binding: FragmentHospitalBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentHospitalBinding.bind(view)

        binding.progressBar.visibility = View.VISIBLE

        val adapter = UnsplashPhotoAdapter()

        binding.apply {
            hospitalRecycler.setHasFixedSize(true)
            hospitalRecycler.adapter = adapter


        }

        viewModel.photo.observe(viewLifecycleOwner, Observer {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)
            binding.progressBar.visibility = View.GONE

        })


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}