package com.example.finalkotlinapp.ui.dentalclinic

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.finalkotlinapp.R
import com.example.finalkotlinapp.databinding.FragmentDentalClinicBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DentalClinicFragment : Fragment(R.layout.fragment_dental_clinic) {

    private val viewModel by viewModels<DentalViewModel>()

    private var _binding: FragmentDentalClinicBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentDentalClinicBinding.bind(view)

        val adapter = UnsplashDentalPhotoAdapter()

        binding.apply {
            dentalRecycler.setHasFixedSize(true)
            dentalRecycler.adapter = adapter
        }
        viewModel.photo.observe(viewLifecycleOwner, Observer {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)

        })


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}