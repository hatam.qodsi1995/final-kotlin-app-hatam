package com.example.finalkotlinapp.ui.communicate

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.finalkotlinapp.databinding.FragmentCommunicateBinding


class CommunicateFragment : Fragment() {


    private var _binding: FragmentCommunicateBinding? = null

    private val binding get() = _binding!!

    private val args : CommunicateFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCommunicateBinding.inflate(layoutInflater, container, false)
        val view = binding.root

        binding.textViewUsername.text = args.username
        binding.textViewPhone.text = args.phoneNumber

        binding.mapsImage.setOnClickListener {
            val intent1 = Intent(Intent.ACTION_VIEW, Uri.parse("geo:35,51?q=University of Tehran"))
            startActivity(intent1)
        }

        binding.smsImage.setOnClickListener {

            val sms = Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:09333139069"))
            startActivity(sms)
        }

        binding.callImage.setOnClickListener {


            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:09333139069")
            startActivity(intent)


        }

        binding.telegramImage.setOnClickListener {

            val telegram = Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/HatamQodsi"))
            startActivity(telegram)


        }









        return (view)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}