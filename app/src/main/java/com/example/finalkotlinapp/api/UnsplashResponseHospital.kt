package com.example.finalkotlinapp.api

import com.example.finalkotlinapp.data.hospital.UnsplashHospitalData

data class UnsplashResponseHospital(
    val results: List<UnsplashHospitalData>
)

