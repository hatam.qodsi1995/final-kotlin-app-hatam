package com.example.finalkotlinapp.api

import com.example.finalkotlinapp.data.dental.UnsplashDentalClinicData

class UnsplashResponseDental(
    val results: List<UnsplashDentalClinicData>
)