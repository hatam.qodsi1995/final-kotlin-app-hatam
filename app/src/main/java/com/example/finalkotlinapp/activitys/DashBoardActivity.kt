package com.example.finalkotlinapp.activitys

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.finalkotlinapp.R
import com.example.finalkotlinapp.databinding.ActivityDashBoardBinding
import com.example.finalkotlinapp.mainFragments.HomeFragment
import com.example.finalkotlinapp.mainFragments.HomeFragmentDirections
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashBoardActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDashBoardBinding

    private lateinit var navController: NavController
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var appBarConfiguration: AppBarConfiguration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashBoardBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setSupportActionBar(binding.dashboardToolbar)

        binding.exitButton.setOnClickListener { exitFromAccount() }
        binding.menuButton.setOnClickListener { openNavigationDrawer() }

        navHostFragment = supportFragmentManager
            .findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        navController = navHostFragment.navController


        binding.dashboardBottomNavigation1.setupWithNavController(navController)

        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.navViewDashboard.setNavigationItemSelectedListener(drawerListener)


    }


    override fun onBackPressed() {

        MaterialAlertDialogBuilder(this)
            .setTitle("! Exit From App !")
            .setMessage("Do You Want To Exit ? ")
            .setPositiveButton(
                "Yes"
            ) { _, _ ->
                finish()
            }
            .setNegativeButton(
                "No"
            ) { dialog, _ -> dialog.dismiss() }
            .show()

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragmentContainerView)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun openNavigationDrawer() {
        if (binding.dashboardDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.dashboardDrawer.closeDrawer(GravityCompat.START)
        } else {
            binding.dashboardDrawer.openDrawer(GravityCompat.START)
        }
    }

    private fun exitFromAccount() {
        val share = getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val editor = share.edit()

        MaterialAlertDialogBuilder(this)
            .setTitle("! Exit From Account !")
            .setMessage("Do You Want To Exit ? ")
            .setPositiveButton(
                "Yes"
            ) { _, _ ->
                editor.apply {
                    putString("STATE_KEY", "false")
                }.apply()
                finish()
            }
            .setNegativeButton(
                "No"
            ) { dialog, _ -> dialog.dismiss() }
            .show()

    }


    private val drawerListener =
        NavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.drawer_exit -> {
                    exitFromAccount()
                }
                R.id.drawer_website -> {
                    val intent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://fanavari.co/android-course/"))
                    startActivity(intent)
                }
                R.id.drawer_telegram -> {
                    val telegram =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/HatamQodsi"))
                    startActivity(telegram)
                }
                R.id.drawer_messsage -> {
                    val sms =
                        Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:09333139069"))
                    startActivity(sms)
                }

            }
            true
        }


}