package com.example.finalkotlinapp.activitys

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.finalkotlinapp.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class SplashScreen : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.M)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val share = getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val state = share.getString("STATE_KEY", null)

        val cm = applicationContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
        val connected = capabilities?.hasCapability(NET_CAPABILITY_INTERNET) == true


        Handler().postDelayed({
            if (state == "true" && connected) {
                val intent = Intent(this, DashBoardActivity::class.java)
                startActivity(intent)
                finish()

            } else if (state == "true" && !connected) {

                MaterialAlertDialogBuilder(this)
                    .setTitle("! ATTENTION !")
                    .setMessage(" Turn on Your Network And Proxy ! ")
                    .setPositiveButton(
                        "ok"
                    ) { dialog, _ ->
                        val intent = Intent(this, DashBoardActivity::class.java)
                        startActivity(intent)
                        dialog.dismiss()
                        finish()

                    }

                    .show()


            } else if (state == "false" && connected) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {

                MaterialAlertDialogBuilder(this)
                    .setTitle("! ATTENTION !")
                    .setMessage(" Turn on Your Network And Proxy ! ")
                    .setPositiveButton(
                        "ok"
                    ) { dialog, _ ->
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                        dialog.dismiss()
                        finish()
                    }

                    .show()
            }
        }, 3000)


    }




}





