package com.example.finalkotlinapp.activitys

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.finalkotlinapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    companion object {
        private const val INTERNET_PERMISSION_CODE = 100
        private const val ACCESS_NETWORK_STATE_PERMISSION_CODE = 101
        private const val READ_PHONE_STATE_PERMISSION_CODE = 102

    }

    private lateinit var binding: ActivityMainBinding



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        checkPermission(
            Manifest.permission.INTERNET, INTERNET_PERMISSION_CODE)
        checkPermission(
            Manifest.permission.ACCESS_NETWORK_STATE, ACCESS_NETWORK_STATE_PERMISSION_CODE)
        checkPermission(
            Manifest.permission.READ_PHONE_STATE, READ_PHONE_STATE_PERMISSION_CODE)


        binding.buttonSignUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        binding.buttonLogin.setOnClickListener {
            loginCheck()
        }


    }

    override fun onBackPressed() {

        finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == INTERNET_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Internet Permission Granted", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this, "Internet Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        } else if (requestCode == ACCESS_NETWORK_STATE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Access Network Permission Granted", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this, "Access Network Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        } else if (requestCode == READ_PHONE_STATE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Read Phone State Permission Granted", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this, "Read Phone State Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }


    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED) {

            ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
        } else {
            Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show()
        }
    }


    private fun loginCheck() {

        val share = getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val editor = share.edit()
        editor.apply {
            putString("STATE_KEY", "true")
        }.apply()
        val savedUserName = share.getString("USER_Key", null)
        val savedPassword = share.getString("PASS_Key", null)

        if (binding.edittextUsername.text.toString() == savedUserName && binding.edittextPassword.text.toString() == savedPassword) {
            val intent = Intent(this, DashBoardActivity::class.java)
            startActivity(intent)
            finish()
        } else if (binding.edittextUsername.text.toString() == "" || binding.edittextPassword.text.toString() == "") {
            Toast.makeText(this, "Enter Your Username or Password", Toast.LENGTH_SHORT).show()
        } else if (binding.edittextUsername.text.toString() != savedUserName || binding.edittextPassword.text.toString() != savedPassword) {
            Toast.makeText(this, "Worng Username or Password", Toast.LENGTH_SHORT).show()
        }


    }

}
