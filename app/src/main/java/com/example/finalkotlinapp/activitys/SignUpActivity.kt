package com.example.finalkotlinapp.activitys

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.finalkotlinapp.databinding.ActivitySignUpBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding

    @SuppressLint("SetTextI18n")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        MaterialAlertDialogBuilder(this)
            .setTitle("! حریم شخصی کاربران !")
            .setMessage( " این برنامه اطلاعات شما را به صورت محلی و در داخل گوشی خودتان دخیره کرده و هیچگونه نگرانی بابت درز اطلاعات هویتی و شماره تلفن شما کاربران عزیز وجود ندارد . ")
            .setPositiveButton(
                "متوجه شدم"
            ) { dialog, _ ->

                dialog.dismiss()
            }

            .show()



        binding.buttonSignUpAccept.setOnClickListener {

            if (binding.editTextSignUpName.text.toString() == "" || binding.editTextSignUpFamily.text.toString() == ""
                || binding.editTextPhoneNumber.text.toString() == "" || binding.editTextSignUpuserName.text.toString() == ""
                || binding.editTextSignUpPassword.text.toString() == ""
                || binding.editTextSignUpReEnterPassword.text.toString() == ""
            ) {

                Toast.makeText(this, "Please Fill The parameter's", Toast.LENGTH_SHORT).show()
            } else if (binding.editTextSignUpPassword.text.toString() == binding.editTextSignUpReEnterPassword.text.toString()) {
                saveUserInformation()
                val intent = Intent(this, DashBoardActivity::class.java)
                startActivity(intent)
                finish()
            } else {

                Toast.makeText(this, "No Match Password", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun saveUserInformation() {
        val nameText: String = binding.editTextSignUpName.text.toString()
        val familyText: String = binding.editTextSignUpFamily.text.toString()
        val phoneNumberText: String = binding.editTextPhoneNumber.text.toString()
        val userNametext: String = binding.editTextSignUpuserName.text.toString()
        val passText: String = binding.editTextSignUpPassword.text.toString()
        val rePassText: String = binding.editTextSignUpReEnterPassword.text.toString()
        val state = "true"

        val share = getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val editor = share.edit()
        editor.apply {
            putString("NAME_Key", nameText)
            putString("FAMILY_Key", familyText)
            putString("PHONE_Key", phoneNumberText)
            putString("USER_Key", userNametext)
            putString("PASS_Key", passText)
            putString("REPASS_Key", rePassText)
            putString("STATE_KEY", state)
        }.apply()
    }

}