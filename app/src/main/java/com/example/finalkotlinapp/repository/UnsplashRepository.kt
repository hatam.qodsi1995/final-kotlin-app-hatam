package com.example.finalkotlinapp.repository

import androidx.paging.*
import com.example.finalkotlinapp.api.UnsplashApi
import com.example.finalkotlinapp.data.dental.UnsplashPagingSourceDental
import com.example.finalkotlinapp.data.hospital.UnsplashPagingSourceHospital
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UnsplashRepository @Inject constructor(private val unsplashApi: UnsplashApi) {

    fun getSearchResultsHospital(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                UnsplashPagingSourceHospital(
                    unsplashApi, query
                )
            }
        ).liveData

    fun getSearchResultsDental(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                UnsplashPagingSourceDental(
                    unsplashApi, query
                )
            }
        ).liveData

}