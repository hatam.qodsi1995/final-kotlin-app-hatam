package com.example.finalkotlinapp.mainFragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.finalkotlinapp.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        val view = binding.root

        binding.buttonProfileAccept.setOnClickListener {

            if (binding.editTextProfileName.text.toString() == "" || binding.editTextProfileFamily.text.toString() == ""
                || binding.editTextProfilePhoneNumber.text.toString() == "" || binding.editTextProfileUserName.text.toString() == ""
                || binding.editTextProfilePassword.text.toString() == ""
                || binding.editTextProfileReEnterPassword.text.toString() == ""
            ) {

                Toast.makeText(
                    this.requireActivity(),
                    "Please Fill The parameter's",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (binding.editTextProfilePassword.text.toString() == binding.editTextProfileReEnterPassword.text.toString()) {
                changeUserInformation()
                val action = ProfileFragmentDirections.actionProfileFragmentToHomeFragment()
                Navigation.findNavController(view).navigate(action)
                Toast.makeText(this.requireActivity(), "Information's Changed", Toast.LENGTH_SHORT)
                    .show()


            } else {

                Toast.makeText(this.requireActivity(), "No Match Password", Toast.LENGTH_SHORT)
                    .show()
            }

        }


        return (view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun changeUserInformation() {
        val nameText: String = binding.editTextProfileName.text.toString()
        val familyText: String = binding.editTextProfileFamily.text.toString()
        val phoneNumberText: String = binding.editTextProfilePhoneNumber.text.toString()
        val userNametext: String = binding.editTextProfileUserName.text.toString()
        val passText: String = binding.editTextProfilePassword.text.toString()
        val rePassText: String = binding.editTextProfileReEnterPassword.text.toString()

        val share = this.requireActivity().getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val editor = share.edit()
        editor.apply {
            putString("NAME_Key", nameText)
            putString("FAMILY_Key", familyText)
            putString("PHONE_Key", phoneNumberText)
            putString("USER_Key", userNametext)
            putString("PASS_Key", passText)
            putString("REPASS_Key", rePassText)
        }.apply()
    }

}