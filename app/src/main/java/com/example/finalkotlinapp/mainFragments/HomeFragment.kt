package com.example.finalkotlinapp.mainFragments


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.finalkotlinapp.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        val view = binding.root

        setUserInformation()

        val share = this.requireActivity()
            .getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val username = share.getString("USER_Key", null)
        val phonenum = share.getString("PHONE_Key", null)


        binding.hospitalCard.setOnClickListener {

            val action = HomeFragmentDirections.actionHomeFragmentToHospitalFragment()
            Navigation.findNavController(view).navigate(action)
        }
        binding.buttonEditFragmentHome.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToProfileFragment()
            Navigation.findNavController(view).navigate(action)
        }

        binding.dentalclinicCards.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToDentalClinicFragment()
            Navigation.findNavController(view).navigate(action)
        }

        binding.communicateCard.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToCommunicateFragment(
                username.toString(),
                phonenum.toString()
            )
            Navigation.findNavController(view).navigate(action)
        }

        binding.websiteCard.setOnClickListener {
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://fanavari.co/android-course/"))
            startActivity(intent)
        }

        binding.updateCard.setOnClickListener {

            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("bazaar://details?id=" + "com.example.finalkotlinapp")
            intent.setPackage("com.farsitel.bazaar")
            startActivity(intent)

        }




        return (view)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setUserInformation() {
        val share = this.requireActivity()
            .getSharedPreferences("sharedData", Context.MODE_PRIVATE)
        val username = share.getString("USER_Key", null)
        val phonenum = share.getString("PHONE_Key", null)

        binding.textViewFragmentHomeUsername.text = username
        binding.textViewFragmentHomePhonenumber.text = phonenum
    }


}